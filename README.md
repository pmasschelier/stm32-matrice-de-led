# STM32 - Matrice de LED

## TP C

https://se203.telecom-paris.fr/tp/objectifs.html

[Sonde JLink](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack)

### Procédure de remise à zéro

```sh
JLinkExe -device STM32L475VG  -speed auto  -if SWD
> connect
> halt
> erase
> mem32 0 128
> exit
```

### Compilation de matrix.elf

```sh
make
```

### Vérification du link

```sh
arm-none-eabi-objdump -f matrix.elf # Permet de voir le point d'entrée
arm-none-eabi-objdump -h matrix.elf # Permet de voir les sections
```

### Éxecution de matrix.elf

```sh
make connect
```

Puis dans un autre terminal :

```sh
make debug
> flash
> cont
```

### Test de l'UART

```sh
make test-uart # Utilise tmux pour afficher le resultat
```

## TP Rust

https://se202.telecom-paris.fr/labs/labs/led-matrix/setup.html