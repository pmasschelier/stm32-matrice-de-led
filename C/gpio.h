#ifndef GPIO_H
#define GPIO_H

/*
00: Input mode
01: General purpose output mode
10: Alternate function mode
11: Analog mode (reset state) 
*/
#define INPUT_MODE 0x00000000
#define OUTPUT_MODE 0x55555555
#define ALTERNATE_MODE 0xAAAAAAAA
#define ANALOG_MODE 0xFFFFFFFF

#define SETMODE(REG, MASK, MODE) (REG = (REG & ~MASK) | (MODE & MASK))

#endif