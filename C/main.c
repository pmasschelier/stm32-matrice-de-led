#include "clocks.h"
#include "led.h"
#include "uart.h"
#include "matrix.h"
#include "stm32l475xx.h"

#define TTY_BAUDRATE 38400 // new : 38400 // previous : 115200
#define IMAGE_SIZE 192 // 64 * 3 = 192
#define FPS 60

void irq_init(void);
void button_init(void);
void timer_init(uint32_t max_us);

uint8_t image[IMAGE_SIZE];

void USART1_IRQHandler(void) {
	static int pos = -1;
	if(pos >= IMAGE_SIZE || USART1->ISR & (USART_ISR_ORE | USART_ISR_FE))
		pos = -1;
	else if(USART1->ISR & USART_ISR_RXNE) {
		uint8_t c = (uint8_t) USART1->RDR;
		if(c == 0xff)
			pos = 0;
		else if(pos >= 0)
			image[pos++] = c;
	}
}

int main(void) {
	clocks_init();

	irq_init();
	button_init();

	init_led();
	uart_init(TTY_BAUDRATE);
	matrix_init();

	timer_init(1000000 / (FPS * ROW_COUNT));

	while(1) ;
	
	return 0;
}
