extern char _text_lda, _data_lda;
extern char _text, _etext, _data,
	_edata, _bstart, _bend;

__attribute__((section(".textrom"))) void init_text(void)
{
	char *dst = &_text;
	char *src = &_text_lda;
	while (dst < &_etext)
		*dst++ = *src++;
}

void init_bss(void)
{
	char *dst= &_bstart;
	while (dst < &_bend)
		*dst++ = 0;
}

void init_data(void)
{
	char *dst = &_data;
	char *src = &_data_lda;
	while (dst < &_edata)
		*dst++ = *src++;
}