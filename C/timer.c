#include "stm32l475xx.h"
#include "matrix.h"

extern rgb_color image[];

void timer_init(uint32_t max_us) {
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
	TIM2->SR = 0x0;
	TIM2->CNT = 0x0;
	TIM2->CR1 &= ~TIM_CR1_DIR; // Counter used as upcounter p. 966
	TIM2->PSC = 79; // Prescaler : f = fclk / (1+ TIM2[PSC])
	TIM2->ARR = max_us;
	TIM2->DIER = TIM_DIER_UIE; // Update interrupt enable

	NVIC_EnableIRQ(TIM2_IRQn);
	TIM2->CR1 |= TIM_CR1_CEN;
}

void TIM2_IRQHandler(void) {
	// Pour switch la led à chaque interruption du timer (appeler (timer_init avec 1.000.000us))
	/* static uint8_t active = 1;
	active ? led_g_on() : led_g_off();
	active = !active; */

	static uint8_t row = 0;
	rgb_color* img = (rgb_color*) image;
	mat_set_row(row, &img[row * ROW_COUNT]);
	row = (row + 1) % ROW_COUNT;

	TIM2->SR &= ~TIM_SR_UIF; // This bit is set by hardware on an update event. It is cleared by software.
}