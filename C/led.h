#ifndef LED_H
#define LED_H

typedef enum {
    LED_OFF,
    LED_YELLOW,
    LED_BLUE
} LED_SWITCH;

void init_led(void);
void led_g_on(void);
void led_g_off(void);
void led(LED_SWITCH state);

#endif