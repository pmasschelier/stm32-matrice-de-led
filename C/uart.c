#include "stm32l475xx.h"

void uart_init(int baudrate) {
	RCC->AHB2ENR = (RCC->AHB2ENR & ~RCC_AHB2ENR_GPIOBEN_Msk) | RCC_AHB2ENR_GPIOBEN; // Activer l'horloge du PORTB
    GPIOB->MODER =  (GPIOB->MODER & ~GPIO_MODER_MODE6_Msk) |  GPIO_MODER_MODE6_1; // Passer la broche RX du port B en mode USART
    GPIOB->MODER =  (GPIOB->MODER & ~GPIO_MODER_MODE7_Msk) |  GPIO_MODER_MODE7_1; // Passer la broche TX du port B en mode USART
    GPIOB->AFR[0] = (GPIOB->AFR[0]  & ~GPIO_AFRL_AFSEL6_Msk) | (7 << GPIO_AFRL_AFSEL6_Pos); // Alternate function = USART
    GPIOB->AFR[0] = (GPIOB->AFR[0]  & ~GPIO_AFRL_AFSEL7_Msk) | (7 << GPIO_AFRL_AFSEL7_Pos);

    RCC->APB2ENR |= RCC_APB2ENR_USART1EN; // Activer l'horloge du port série USART1

    RCC->CCIPR &= ~RCC_CCIPR_USART1SEL; // Spécifier que l'horloge sur laquelle se base l'USART1 pour timer chaque bit est PCLK

    RCC->APB2RSTR |=  RCC_APB2RSTR_USART1RST; // Faire un reset du port série
    RCC->APB2RSTR &= ~RCC_APB2RSTR_USART1RST;

    USART1->BRR = 80000000 / baudrate; // Configurer la vitesse du port série à 115200 bauds

    //USART1->CR2 &= ~(USART_CR2_STOP_0 | USART_CR2_STOP_1); // Mettre le port série en mode 8N1

    //USART1->CR1 &= ~USART_CR1_OVER8; // Configurer l'oversampling à 16
    USART1->CR1 |= USART_CR1_UE | USART_CR1_RE | USART_CR1_TE | USART_CR1_RXNEIE; // Activer l'USART1, le transmetteur et le récepteur ainsi que le RXNE interrupt
	USART1->CR3 |= USART_CR3_EIE; // Error interrupt enable p. 1388

	NVIC_EnableIRQ(USART1_IRQn); // stm32l475xx_rm.pdf 13.3
}

void uart_putchar(uint8_t c) {
    while(! (USART1->ISR & USART_ISR_TXE) );
    USART1->TDR = c;
}

uint8_t uart_getchar() {
    while(! (USART1->ISR & USART_ISR_RXNE) );
    return (uint8_t) USART1->RDR;
}

void uart_puts(const char *s) {
	unsigned i = 0;
    while(s[i] != '\0') {
        uart_putchar(s[i++]);
    }
    uart_putchar('\n');
}

void uart_gets(char *s, unsigned size) {
	char c = '\0';
	unsigned i; 
    for(i = 0; i < size && c != '\r' && c != '\n'; i++) {
        c = uart_getchar();
        s[i] = c;
    }
	s[i] = '\0';
}