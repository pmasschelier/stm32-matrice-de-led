	.syntax unified
	.global _start
	.thumb
	.thumb_func
	.section .textrom, "ax", %progbits
_start:
	ldr sp, =_stack
	bl init_text
	bl init_bss
	bl init_data
	bl main

_exit:
	b _exit
