
#include "stm32l475xx.h"
#include "matrix.h"
#include <stdint.h>

uint32_t rstdelay = 8000000UL; // À 80MHz on fait 8'000'000 d'opérations pour attendre 100ms
uint32_t regdelay = 3UL; // Limite à 20MHz
uint32_t leddelay = 70UL;

#define RST(x) (GPIOC->BSRR = ((x) ? GPIO_BSRR_BS3 : GPIO_BSRR_BR3))
#define SB(x) (GPIOC->BSRR = ((x) ? GPIO_BSRR_BS5 : GPIO_BSRR_BR5))
#define LAT(x) (GPIOC->BSRR = ((x) ? GPIO_BSRR_BS4 : GPIO_BSRR_BR4))
#define SCK(x) (GPIOB->BSRR = ((x) ? GPIO_BSRR_BS1 : GPIO_BSRR_BR1))
#define SDA(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS4 : GPIO_BSRR_BR4))
#define ROW0(x) (GPIOB->BSRR = ((x) ? GPIO_BSRR_BS2 : GPIO_BSRR_BR2))
#define ROW1(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS15 : GPIO_BSRR_BR15))
#define ROW2(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS2 : GPIO_BSRR_BR2))
#define ROW3(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS7 : GPIO_BSRR_BR7))
#define ROW4(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS6 : GPIO_BSRR_BR6))
#define ROW5(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS5 : GPIO_BSRR_BR5))
#define ROW6(x) (GPIOB->BSRR = ((x) ? GPIO_BSRR_BS0 : GPIO_BSRR_BR0))
#define ROW7(x) (GPIOA->BSRR = ((x) ? GPIO_BSRR_BS3 : GPIO_BSRR_BR3))

void pulse_SCK() {
	SCK(0);
	for (uint32_t i=0; i<regdelay; i++)
		asm volatile("nop");
	SCK(1);
	for (uint32_t i=0; i<regdelay; i++)
		asm volatile("nop");
}


void pulse_LAT() {
	LAT(1);
	for (uint32_t i=0; i<regdelay; i++)
		asm volatile("nop");
	LAT(0);
	for (uint32_t i=0; i<regdelay; i++)
		asm volatile("nop");
}

void send_byte(uint8_t val, int bank) {
	SB(bank); // SB : 0 pour BANK0, 1 pour BANK1
	for(int i = (bank ? 7 : 5); i >= 0; i--) { // BANK0 stocke 6 bits par LED, BANK1 stocke 8 bits par LED
		SDA((val >> i) & 0x1);
		pulse_SCK();
	}
}

void init_bank0() {
	for(int i = 0; i < 24; i++)
		send_byte(0xFF, 0);
	pulse_LAT();
}

void matrix_init() {
	RCC->AHB2ENR = (RCC->AHB2ENR & ~RCC_AHB2ENR_GPIOAEN_Msk) | RCC_AHB2ENR_GPIOAEN; // Activer l'horloge du PORTA
	RCC->AHB2ENR = (RCC->AHB2ENR & ~RCC_AHB2ENR_GPIOBEN_Msk) | RCC_AHB2ENR_GPIOBEN; // Activer l'horloge du PORTB
	RCC->AHB2ENR = (RCC->AHB2ENR & ~RCC_AHB2ENR_GPIOCEN_Msk) | RCC_AHB2ENR_GPIOCEN; // Activer l'horloge du PORTC

	RST(0);

	// Broche du processeur = Broche du driver
	// PC5 = SB
    GPIOC->MODER =  (GPIOC->MODER & ~GPIO_MODER_MODE5_Msk) |  GPIO_MODER_MODE5_0; // PC5 -> GPIO OUTPUT
	GPIOC->OSPEEDR |= GPIO_OSPEEDR_OSPEED5_Msk; // Vitesse maximale

	// PC4 = LAT
    GPIOC->MODER =  (GPIOC->MODER & ~GPIO_MODER_MODE4_Msk) |  GPIO_MODER_MODE4_0; // PC4 -> GPIO OUTPUT
	GPIOC->OSPEEDR |= GPIO_OSPEEDR_OSPEED4_Msk; // Vitesse maximale

	// PC3 = RST
    GPIOC->MODER =  (GPIOC->MODER & ~GPIO_MODER_MODE3_Msk) |  GPIO_MODER_MODE3_0; // PC3 -> GPIO OUTPUT
	GPIOC->OSPEEDR |= GPIO_OSPEEDR_OSPEED3_Msk; // Vitesse maximale

	// PB1 = SCK
    GPIOB->MODER =  (GPIOB->MODER & ~GPIO_MODER_MODE1_Msk) |  GPIO_MODER_MODE1_0; // PB1 -> GPIO OUTPUT
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEED1_Msk; // Vitesse maximale

	// PA4 = SDA
    GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE4_Msk) |  GPIO_MODER_MODE4_0; // PA4 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED4_Msk; // Vitesse maximale

	// PB2 = C0
    GPIOB->MODER =  (GPIOB->MODER & ~GPIO_MODER_MODE2_Msk) |  GPIO_MODER_MODE2_0; // PCB -> GPIO OUTPUT
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEED2_Msk; // Vitesse maximale

	// PA15 = C1
    GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE15_Msk) |  GPIO_MODER_MODE15_0; // PA15 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED15_Msk; // Vitesse maximale

	// PA2 = C2
    GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE2_Msk) |  GPIO_MODER_MODE2_0; // PA2 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED2_Msk; // Vitesse maximale

	// PA7 = C3
	GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE7_Msk) |  GPIO_MODER_MODE7_0; // PA7 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED7_Msk; // Vitesse maximale

	// PA6 = C4
    GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE6_Msk) |  GPIO_MODER_MODE6_0; // PA6 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED6_Msk; // Vitesse maximale

	// PA5 = C5
    GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE5_Msk) |  GPIO_MODER_MODE5_0; // PA5 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED5_Msk; // Vitesse maximale

	// PB0 = C6
    GPIOB->MODER =  (GPIOB->MODER & ~GPIO_MODER_MODE0_Msk) |  GPIO_MODER_MODE0_0; // PB0 -> GPIO OUTPUT
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEED0_Msk; // Vitesse maximale

	// PA3 = C7
    GPIOA->MODER =  (GPIOA->MODER & ~GPIO_MODER_MODE3_Msk) |  GPIO_MODER_MODE3_0; // PA3 -> GPIO OUTPUT
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED3_Msk; // Vitesse maximale

	LAT(1);
	SB(1);
	SCK(0);
	SDA(0);

	deactivate_rows();

	for (uint32_t i=0; i<rstdelay; i++)
		asm volatile("nop");
	
	RST(1);

	init_bank0();

}

void deactivate_rows() {
	ROW0(0);
	ROW1(0);
	ROW2(0);
	ROW3(0);
	ROW4(0);
	ROW5(0);
	ROW6(0);
	ROW7(0);
}

void activate_row(int row) {
	switch(row) {
		case 0: ROW0(1); break;
		case 1: ROW1(1); break;
		case 2: ROW2(1); break;
		case 3: ROW3(1); break;
		case 4: ROW4(1); break;
		case 5: ROW5(1); break;
		case 6: ROW6(1); break;
		case 7: ROW7(1); break;
		default: break;
	}
}

void mat_set_row(int row, const rgb_color *val) {
	for(int i = 7; i >= 0; i--) {
		send_byte(val[i].b, 1);
		send_byte(val[i].g, 1);
		send_byte(val[i].r, 1);
	}
	deactivate_rows();
	for (uint32_t i=0; i<leddelay; i++)
		asm volatile("nop");
	pulse_LAT();
	activate_row(row);
}

void display_image(rgb_color* img) {
	for(int i = 0; i < 8; i++)
		mat_set_row(i, &img[i * 8]);
}