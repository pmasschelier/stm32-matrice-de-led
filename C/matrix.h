#ifndef MATRIX_H
#define MATRIX_H

#include <stdint.h>

#define ROW_COUNT 8

typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb_color;

void matrix_init();
void deactivate_rows();
void activate_row(int row);
void mat_set_row(int row, const rgb_color *val);
void display_image(rgb_color* img);

#endif