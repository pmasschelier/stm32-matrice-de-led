#include "stm32l475xx.h"
#include "led.h"
#include "gpio.h"

void init_led(void) {
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN | RCC_AHB2ENR_GPIOCEN;
	SETMODE(GPIOB->MODER, GPIO_MODER_MODE14, OUTPUT_MODE);
	SETMODE(GPIOC->MODER, GPIO_MODER_MODE9, INPUT_MODE);
}

void led_g_on(void) {
    GPIOB->BSRR = GPIO_BSRR_BS14;

}

void led_g_off(void) {
    GPIOB->BSRR = GPIO_BSRR_BR14;
}

void led(LED_SWITCH state) {
    switch (state)
    {
    case LED_OFF:
		SETMODE(GPIOC->MODER, GPIO_MODER_MODE14, INPUT_MODE);
        break;
    case LED_YELLOW:
		SETMODE(GPIOC->MODER, GPIO_MODER_MODE9, OUTPUT_MODE);
        GPIOC->BSRR = GPIO_BSRR_BS9;
        break;
    case LED_BLUE:
		SETMODE(GPIOC->MODER, GPIO_MODER_MODE9, OUTPUT_MODE);
        GPIOC->BSRR = GPIO_BSRR_BR9;
        break;
    default:
        break;
    }
}