#include "stm32l475xx.h"
#include "led.h"
#include "gpio.h"

void button_init(void) {
	RCC->AHB2ENR = (RCC->AHB2ENR & ~RCC_AHB2ENR_GPIOCEN_Msk) | RCC_AHB2ENR_GPIOCEN; // Activer l'horloge du PORTC
    GPIOC->MODER = SETMODE(GPIOC->MODER, GPIO_MODER_MODE13, INPUT_MODE); // PC13 -> GPIO INPUT
	SYSCFG->EXTICR[3] = (SYSCFG->EXTICR[3] & ~SYSCFG_EXTICR4_EXTI13) | SYSCFG_EXTICR4_EXTI13_PC;

	/* stm32l475xx_rm.pdf 14.3.4 */
	EXTI->IMR1 |= EXTI_IMR1_IM13;
	EXTI->RTSR1 &= ~EXTI_RTSR1_RT13;
	EXTI->FTSR1 |= EXTI_FTSR1_FT13;
	NVIC_EnableIRQ(EXTI15_10_IRQn); // stm32l475xx_rm.pdf 13.3
}

void EXTI15_10_IRQHandler(void) {
	static uint8_t active = 1;
	active ? led_g_on() : led_g_off();
	active = !active;
	EXTI->PR1 = (EXTI->PR1 & ~EXTI_PR1_PIF13_Msk) | EXTI_PR1_PIF13;
}