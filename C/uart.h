#ifndef USART_H
#define USART_H

#include <stdint.h>

void uart_init(int baudrate);
void uart_putchar(uint8_t c);
uint8_t uart_getchar();
void uart_puts(const char *s);
void uart_gets(char *s, unsigned size);

#endif