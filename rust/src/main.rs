#![no_std]
#![no_main]

use stm32l4xx_hal::serial::{Config, Event, Rx, Serial};
use stm32l4xx_hal::{pac, prelude::*};

use defmt_rtt as _;
use dwt_systick_monotonic::{DwtSystick, ExtU32};
use heapless::pool::{Box, Node, Pool};
use panic_probe as _;
use embedded_graphics::{
	prelude::{Dimensions, Point },
	pixelcolor::Rgb888,
	Drawable,
	text::Text,
};

use tp_led_matrix::{Image, Color};
mod matrix;
use matrix::Matrix;

#[rtic::app(device = pac, dispatchers = [USART2, USART3, UART4])]
mod app {
    use embedded_graphics::mono_font::{MonoTextStyle, self};

    use super::*;

    #[monotonic(binds = SysTick, default = true)]
    type MyMonotonic = DwtSystick<80_000_000>;
    type Instant = <MyMonotonic as rtic::Monotonic>::Instant;

    #[shared]
    struct Shared {
        next_image: Option<Box<Image>>,
        pool: Pool<Image>,
        changes: u32,
    }

    #[local]
    struct Local {
        matrix: Matrix,
        usart1_rx: Rx<pac::USART1>,
        current_image: Box<Image>,
        rx_image: Box<Image>,
    }

    #[init]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        use core::mem::MaybeUninit;

        defmt::info!("defmt correctly initialized");

        let mut cp = cx.core;
        let dp = cx.device;

        let mut mono = DwtSystick::new(&mut cp.DCB, cp.DWT, cp.SYST, 80_000_000);

        // Get high-level representations of hardware modules
        let mut rcc = dp.RCC.constrain();
        let mut flash = dp.FLASH.constrain();
        let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

        // Setup the clocks at 80MHz using HSI (by default since HSE/MSI are not configured).
        // The flash wait states will be configured accordingly.
        let clocks = rcc.cfgr.sysclk(80.MHz()).freeze(&mut flash.acr, &mut pwr);

        let mut gpioa = dp.GPIOA.split(&mut rcc.ahb2);
        let mut gpiob = dp.GPIOB.split(&mut rcc.ahb2);
        let mut gpioc = dp.GPIOC.split(&mut rcc.ahb2);

        let serial_tx =
            gpiob
                .pb6
                .into_alternate::<7>(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrl);
        let serial_rx =
            gpiob
                .pb7
                .into_alternate::<7>(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrl);

        let config = Config::default().baudrate(38400.bps());

        let mut serial = Serial::usart1(
            dp.USART1,
            (serial_tx, serial_rx),
            config,
            clocks,
            &mut rcc.apb2,
        );

        serial.listen(Event::Rxne);

        let usart1_rx = serial.split().1;

        let matrix = Matrix::new(
            gpioa.pa2,
            gpioa.pa3,
            gpioa.pa4,
            gpioa.pa5,
            gpioa.pa6,
            gpioa.pa7,
            gpioa.pa15,
            gpiob.pb0,
            gpiob.pb1,
            gpiob.pb2,
            gpioc.pc3,
            gpioc.pc4,
            gpioc.pc5,
            &mut gpioa.moder,
            &mut gpioa.otyper,
            &mut gpiob.moder,
            &mut gpiob.otyper,
            &mut gpioc.moder,
            &mut gpioc.otyper,
            clocks,
        );

        let pool: Pool<Image> = Pool::new();
        unsafe {
            static mut MEMORY: MaybeUninit<[Node<Image>; 3]> = MaybeUninit::uninit();
            pool.grow_exact(&mut MEMORY); // static mut access is unsafe
        }

        let current_image = pool.alloc().unwrap().init(Image::default());
        let rx_image = pool.alloc().unwrap().init(Image::default());
        let next_image = None;

        let now = mono.now();
        display::spawn(now).unwrap();
        screensaver::spawn().unwrap();

        // Return the resources and the monotonic timer
        (
            Shared {
                pool,
                next_image,
                changes: 0,
            },
            Local {
                matrix,
                usart1_rx,
                rx_image,
                current_image,
            },
            init::Monotonics(mono),
        )
    }

    #[idle(local = [counter: u32 = 0])]
    fn idle(_cx: idle::Context) -> ! {
        loop {
            /* if *cx.local.counter == 10_000 {
                defmt::info!("Idle task ran a lot !");
                *cx.local.counter = 0;
            } else {
                *cx.local.counter += 1;
            } */
        }
    }

    #[task(shared = [changes], priority = 3)]
    fn notice_change(mut cx: notice_change::Context) {
        cx.shared.changes.lock(|changes| {
            *changes += 1;
        });
    }

    #[task(shared = [pool, changes, next_image], local = [last_changes: u32 = 0, color_index: usize = 0, offsetx: i32 = 8])]
    fn screensaver(mut cx: screensaver::Context) {
		const COLORS : [Color; 3] = [Color::RED, Color::GREEN, Color::BLUE];
		let color_index = cx.local.color_index;

        cx.shared.changes.lock(|changes| {
            let last_changes = cx.local.last_changes;
			let offset = cx.local.offsetx;
			

            if *last_changes == *changes {
                (cx.shared.next_image, cx.shared.pool).lock(|next_image, pool| {
                    // Replace the image content by the new one, for example
                    // by swapping them, and reset next_pos
                    if let Some(image) = next_image.take() {
                        pool.free(image);
                    }
                    let mut future_image = pool.alloc().unwrap().init(Image::default());
					
					let c = COLORS[*color_index];
					let text = Text::new(
						"Hello SE202", 
						Point::new(*offset, 7), 
						MonoTextStyle::new(&mono_font::ascii::FONT_5X8, Rgb888::new(c.r, c.g, c.b))
					);
					text.draw(&mut *future_image).unwrap();
                    *next_image = Some(future_image);

					let end = -*offset > text.bounding_box().size.width as i32;
					if end { *color_index = (*color_index + 1) % COLORS.len(); }
					*offset = if end { 8 } else { *offset - 1 };
                });
            }
			else {
				*offset = 0;
			}
            *last_changes = *changes;
        });

        screensaver::spawn_after(60.millis()).unwrap();
    }

    #[task(local = [matrix, current_image, next_line: usize = 0], shared = [pool, next_image], priority = 2)]
    fn display(cx: display::Context, at: Instant) {
        use core::mem::swap;
        // Display line next_line (cx.local.next_line) of
        // the image (cx.local.image) on the matrix (cx.local.matrix).
        // All those are mutable references.

        let line: &mut usize = cx.local.next_line;
        let current_image = cx.local.current_image;

        cx.local.matrix.send_row(*line, current_image.row(*line));

        // Increment next_line up to 7 and wraparound to 0
        *line += 1;
        if *line == 8 {
            (cx.shared.next_image, cx.shared.pool).lock(|next_image, pool| {
                if let Some(mut image) = next_image.take() {
                    swap(&mut image, current_image);
                    pool.free(image);
                };
            });
            *line = 0;
        }

        let period = 1.secs() / 8 / 60;
        let next_time = at + period;
        display::spawn_at(next_time, next_time).unwrap();
    }

    #[task(binds = USART1,
		local = [usart1_rx, rx_image, next_pos: usize = 0, begin: bool = false],
		shared = [pool, next_image])]
    fn receive_byte(cx: receive_byte::Context) {
        use core::mem::swap;

        let rx_image = cx.local.rx_image;
        let next_pos: &mut usize = cx.local.next_pos;
        const SIZE: usize = 8 * 8 * 3;

        if let Ok(b) = cx.local.usart1_rx.read() {
            // Handle the incoming byte according to the SE203 protocol
            // and update next_image
            // Do not forget that next_image.as_mut() might be handy here!
            defmt::debug!("Received byte {:x}", b);

            if b == 0xff {
                *next_pos = 0;
                *cx.local.begin = true;
                defmt::debug!("Begin new image");
            } else if *cx.local.begin {
                rx_image.as_mut()[*next_pos] = b;
                *next_pos += 1;
            }

            // If the received image is complete, make it available to
            // the display task.
            if *next_pos == SIZE {
                (cx.shared.next_image, cx.shared.pool).lock(|next_image, pool| {
                    // Replace the image content by the new one, for example
                    // by swapping them, and reset next_pos
                    if let Some(image) = next_image.take() {
                        pool.free(image);
                    }
                    let mut future_image = pool.alloc().unwrap().init(Image::default());
                    swap(&mut future_image, rx_image);
                    *next_image = Some(future_image);

                    notice_change::spawn().unwrap();

                    *next_pos = 0;
                    *cx.local.begin = false;
                });
            }
        }
    }
}
