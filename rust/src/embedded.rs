use core::convert::Infallible;

use crate::{Color, Image};
use embedded_graphics::{
    pixelcolor::Rgb888,
    prelude::{DrawTarget, RgbColor, OriginDimensions, Size}, Pixel,
};

impl From<Rgb888> for Color {
    fn from(value: Rgb888) -> Self {
        Color {
            r: value.r(),
            g: value.g(),
            b: value.b(),
        }
    }
}

impl DrawTarget for Image {
    type Color = Rgb888;
    type Error = Infallible;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
    {
        for Pixel(coord, color) in pixels.into_iter() {
            // Check if the pixel coordinates are out of bounds (negative or greater than
            // (7,7)). `DrawTarget` implementation are required to discard any out of bounds
            // pixels without returning an error or causing a panic.
            if let Ok((x @ 0..=7, y @ 0..=7)) = coord.try_into() {
                self[(x as usize, y as usize)] = color.into();
            }
        }

        Ok(())
    }
}

impl OriginDimensions for Image {
    fn size(&self) -> Size {
        Size::new(8, 8)
    }
}